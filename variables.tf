variable "should_provision" {
  description = "If set to true, create a NAT instance"
  type        = "string"
}

variable "vpc_id" {
  type = "string"
}

variable "vpc_cidr_block" {
  type = "string"
}

variable "private_subnets_cidr_blocks" {
  type = "list"
}

variable "private_route_table_ids" {
  type = "list"
}

variable "public_subnet" {
  type = "string"
}
