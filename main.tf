provider "aws" {}

resource "aws_security_group" "nat" {
  count       = "${var.should_provision}"
  name        = "vpc_nat"
  description = "Allow traffic to pass from the private subnet to the internet"

  # Accept HTTP requests from our private subnets
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["${var.private_subnets_cidr_blocks}"]
  }

  # Accept HTTPS requests from our private subnets
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["${var.private_subnets_cidr_blocks}"]
  }

  # Accept SSH requests from our private subnets
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Accept ICMP requests from our private subnets
  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allows outbound HTTP access to any IPv4 address
  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allows outbound HTTPS access to any IPv4 address
  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allows outbound SSH access to any IPv4 address
  egress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${var.vpc_cidr_block}"]
  }

  # Allows outbound ICMP access to any IPv4 address
  egress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags {
    Name = "NATSG"
  }
}

# Create just 1 NAT instance in the first public subnet
resource "aws_instance" "nat" {
  count = "${var.should_provision}"

  # this is a special ami preconfigured to do NAT
  # aws ec2 describe-images --filter Name="owner-alias",Values="amazon" --filter Name="name",Values="amzn-ami-vpc-nat*"
  ami = "ami-00a9d4a05375b2763"

  availability_zone = "us-east-1a"
  instance_type     = "t2.micro"

  #  key_name                    = "aws"
  vpc_security_group_ids      = ["${aws_security_group.nat.id}"]
  subnet_id                   = "${var.public_subnet}"
  associate_public_ip_address = true
  source_dest_check           = false

  tags {
    Name = "VPC NAT"
  }
}

resource "aws_route" "private_nat_route" {
  count                  = "${var.should_provision ? length(var.private_route_table_ids) : 0}"
  route_table_id         = "${element(var.private_route_table_ids, count.index)}"
  destination_cidr_block = "0.0.0.0/0"
  instance_id            = "${aws_instance.nat.id}"
}

resource "aws_eip" "nat" {
  count    = "${var.should_provision}"
  instance = "${element(aws_instance.nat.*.id, count.index)}"
  vpc      = true
}
